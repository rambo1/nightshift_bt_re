#!/usr/bin/env python3
"""Tests to send commands to Lighmodes Night Shift LED blinker"""
from typing import Any
import asyncio
import uuid
from bleak import BleakScanner, BleakClient

NOTIFY_UUID = "beb5483e-36e1-4688-b7f5-ea07361b26a8"
WRITE_UUID = "4fafc201-1fb5-459e-8fcc-c5c9c331914b"
SCAN_SERVICE_UUIDS = [WRITE_UUID]

async def scan_devices():
    devices = await BleakScanner.discover(service_uuids=SCAN_SERVICE_UUIDS)
    for d in devices:
        print(d)
        print(d.metadata)


async def services():
    devices = await BleakScanner.discover(service_uuids=SCAN_SERVICE_UUIDS, timeout=1.0)
    for dev in devices:
        async with BleakClient(dev) as client:
            print(f"{repr(client)}")
            svcs = await client.get_services()
            print("Services:")
            for service in svcs:
                print(service)


async def write_helper(client: Any, char: Any, cmd: str) -> Any:
    resp = await client.write_gatt_char(char, cmd.encode("ASCII"), response=True)
    print(repr(resp))
    return resp


def handle_notify(hdl: int, data: bytearray):
    print(f"received on {hdl}: {repr(data)}")


async def write():
    devices = await BleakScanner.discover(service_uuids=SCAN_SERVICE_UUIDS, timeout=1.0)
    for dev in devices:
        async with BleakClient(dev) as client:
            print(f"Starting notify on {NOTIFY_UUID}")
            await client.start_notify(NOTIFY_UUID, handle_notify)
            print(f"writing to prev")
            await client.write_gatt_char(NOTIFY_UUID, b"\0")

            srvs = await client.get_services()
            srv = srvs.get_service(WRITE_UUID)
            chars = srv.characteristics
            print(f"srv: {repr(srv)}, chars: {repr(chars)}")
            for char in chars:
                print(f"char: {repr(char)} -> str: {char}")



            await write_helper(client, NOTIFY_UUID, "BRIT 080")
            await asyncio.sleep(2)
            await write_helper(client, NOTIFY_UUID, "BRIT 015")
            await asyncio.sleep(2)
            await write_helper(client, NOTIFY_UUID, "BRIT 080")




if __name__ == "__main__":
   #asyncio.get_event_loop().run_until_complete(services())
   asyncio.get_event_loop().run_until_complete(write())
